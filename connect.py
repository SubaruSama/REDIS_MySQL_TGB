#!/usr/bin/python3

"""
connect.py - Python implementation for TGB.

Python implementation using NoSQL database REDIS
    for TGB - Banco de Dados.
"""

import redis
import sys


class Banco:
    """Class to connect and do the stuff on redis."""

    def __init__(self, address, port, db):
        """
        __init__ method.

        When this class is instantiated create the object with the
            following attributes.
        """
        self.address = address
        self.port = port
        self.db = db
        self.connection = redis.StrictRedis(self.address, self.port, self.db)

    def set_hash(self, key, field, value):
        """
        set_hash method.

        This method allows to write on REDIS
            using the data structure hash.
        """
        self.key = key
        self.field = field
        self.value = value
        self.set = redis.StrictRedis().hset(self.key, self.field, self.value)

    def getall_hash(self, key):
        """
        getall_hash method.

        This method allows to retrieve the
        info inside the datastructure hash on REDIS database.
        """
        self.key = key
        self.get = redis.StrictRedis().hgetall(self.key)
        return self.get

    def get_hash(self, name, key):
        """
        get_hash method.

        This method allows to get only the values of a given key.
        """
        self.name = name
        self.key = key
        self.get = redis.StrictRedis().hget(self.name, self.key)
        return self.get

    def calcular_nota(self, grauA, grauB):
        """
        calcular_nota method.

        This method make the computation of the final note using the
            grauA and grauB.
        """
        self.grauA = grauA
        self.grauB = grauB
        return (self.grauA + (self.grauB * 2)) / 3

def add_to_hash():
    """Function to add values to the hash."""
    user_key = str(input("Enter the key: "))
    user_field = str(input("Enter the field: "))
    user_value = str(input("Enter the value: "))
    banco_redis.set_hash(user_key, user_field, user_value)
    print(banco_redis.getall_hash(user_key))

def computate_final_note():
    """Function to calculate the final note in the context of the program, not the class."""
    name = str(input("Enter the name of the hash: "))
    key_grau_A = "nota_grau_A"
    key_grau_B = "nota_grau_B"
    grauA_bytes = banco_redis.get_hash(name, key_grau_A)
    grauB_bytes = banco_redis.get_hash(name, key_grau_B)
    # We need to convert those two variables.
    # The method of this library returns in byte and we need in int.
    grauA_int = int(grauA_bytes)
    grauB_int = int(grauB_bytes)
    print(grauA_int, grauB_int)
    nota_final = banco_redis.calcular_nota(grauA_int, grauB_int)
    print("Final note of the semester: {:.2f}".format(nota_final))
    if nota_final >= 6:
        print("Congratulations, you've passed!")
    else:
        print("Sorry, you'll need to do the class again :/")
    

grauA = 0
grauB = 0
options = 0
user_address = str(input("Enter the address: "))
user_port = int(input("Enter the port (DEFAULT: 6379): "))
user_db = int(input("Enter the db (DEFAULT: 0): "))

banco_redis = Banco(user_address, user_port, user_db)

try:
    while True:
        print("1: Add values to the hash\n2: Computate the final note")
        options = int(input("Enter your choice: "))

        if options == 1:
            add_to_hash()

        elif options == 2:
            computate_final_note()

except KeyboardInterrupt:
    print("Quitting")
    sys.exit()
