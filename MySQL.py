"""Implementation for the TGB of Banco de Dados I."""

# !/usr/bin/python3
# -*- coding: utf-8 -*-

# Connect to a MySQL Database
# and retrieve info of Aluno table inside TGB Database

# Connect to the database

import pymysql.cursors
import sys
from calculo import Grau

def escolhas():
    """
    Menu de escolhas em o que fazer com o banco.

    Opcao 1: ira retornar os dados de uma tabela.
    Opcao 2: ira inserir dados em uma tabela.
    """
    # 1: retornar tudo sobre uma tabela
    # 2: adicionar valores

    print("\t1: Retornar tudo sobre uma tabela \
          \n\t2: Adicionar valores a uma tabela \
          \n\t3: Retornar apenas as notas de um numero de matricula")

    while True:

        option = int(input("Coloque a sua opcao: "))

        if option == 0:
            print("Saindo...")
            sys.exit()

        if option == 1:
            nomeTabela = str(input("Entre o nome da tabela: "))
            select(nomeTabela)

        if option == 2:
            nomeTabela = str(input("Entre o nome da tabela: "))

            insert(nomeTabela)

        if option == 3:
            idProva = int(input("Entre o numero do id da prova: "))
            print("Retornando apenas as notas da prova com o ID: {}".format(idProva))
            notas(idProva)


def notas(idProva):
    """Retornar apenas as notas da prova de um determinado numero de matricula de um aluno."""
    grauA = 0
    grauB = 0
    connection = pymysql.connect(host='localhost', user='TGB',
                                 password='TGB123456789',
                                 db='TGB',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors
                                                    .DictCursor)
    sql_command = "SELECT `ValorNota`, `Grau` FROM `Prova` WHERE `Aluno_NumeroMatricula`=%s"

    try:
        with connection.cursor() as cursor:
            cursor.execute(sql_command, (idProva,))
            result = cursor.fetchall()
            for i in result:
                print('Grau: {} Valor Nota: {}'.format(i['Grau'], i['ValorNota']))
                if i['Grau'] == 'A':
                    grauA = i['ValorNota']
                elif i['Grau'] == 'B':
                    grauB = i['ValorNota']

            notaFinal = Grau(grauA, grauB)
            print("Nota final: {:.2f}".format(notaFinal.calculo_final()))
    finally:
        connection.close()
        sys.exit()


def select(nomeTabela):
    u"""
    Função que irá utilizar o comando SELECT do SQL.

    Essa função recebe como parâmetro um argumento: nomeTabela
    Com essa informação podemos montar o que o comando SELECT irá fazer.
    """
    sql_command = "SELECT `*` FROM {}".format(nomeTabela)

    connection = pymysql.connect(host='localhost', user='TGB',
                                 password='TGB123456789',
                                 db='TGB',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors
                                                    .DictCursor)
    if nomeTabela == "Aluno":
        try:
            with connection.cursor() as cursor:
                print(sql_command)
                cursor.execute(sql_command)
                # the fetchall() method returns a dictionary
                result = cursor.fetchall()
                for i in result:
                    print("=== === ===")
                    print("Nome: {nome}\nNumeroMatricula: {numMatricula}\nCadeira: {cadeira}"
                          .format(nome=i['Nome'], cadeira=i['Cadeira'],
                                  numMatricula=i['NumeroMatricula']))
                print("=== === ===")

        finally:
            connection.close()
            escolhas()

    elif nomeTabela == "Professor":
        try:
            with connection.cursor() as cursor:
                print(sql_command)
                cursor.execute(sql_command)
                # the fetchall() method returns a dictionary
                result = cursor.fetchall()
                for i in result:
                    print("=== === ===")
                    print("IdProfessor: {idProfessor}\nNome: {nome}"
                          .format(nome=i['Nome'],
                                  idProfessor=i['IdProfessor']))
                print("=== === ===")

        finally:
            connection.close()
            escolhas()

    elif nomeTabela == "Prova":
        try:
            with connection.cursor() as cursor:
                print(sql_command)
                cursor.execute(sql_command)
                # the fetchall() method returns a dictionary
                result = cursor.fetchall()
                for i in result:
                    print("=== === ===")
                    print("IdProva: {}\nValorNota: {}\nGrau: {}\nAluno Numero Matricula: {}\nID Professor: {}".format(i['IdProva'], i['ValorNota'], i['Grau'], i['Aluno_NumeroMatricula'], i['Professor_IdProfessor']))
                print("=== === ===")

        finally:
            connection.close()
            escolhas()


def insert(nomeTabela):
    u"""Função que irá adicionar dados em uma tabela."""
    connection = pymysql.connect(host='localhost', user='TGB',
                                 password='TGB123456789',
                                 db='TGB',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors
                                                    .DictCursor)
    if nomeTabela == 'Aluno':
        numMatricula = int(input("Entre o numero da matricula: "))
        cadeira = str(input("Entre o nome da cadeira: "))
        nome = str(input("Entre o nome do aluno: "))
        sql_command = "INSERT INTO `Aluno` (`NumeroMatricula`, `Cadeira`, `Nome`) VALUES (%s, %s, %s)"
        print("String de consulta: INSERT INTO `Aluno` (`NumeroMatricula`, `Cadeira`, `Nome`) VALUES ({matricula}, {cadeira}, {nome})"
              .format(matricula=numMatricula, cadeira=cadeira, nome=nome))
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql_command, (numMatricula,
                                             cadeira,
                                             nome))
            connection.commit()

        finally:
            connection.close()
            escolhas()

    elif nomeTabela == 'Professor':
        idProfessor = int(input("Entre o ID do professor: "))
        nome = str(input("Entre o nome do professor: "))
        sql_command = "INSERT INTO `Professor` (`IdProfessor`, `Nome`) VALUES (%s, %s)"
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql_command, (idProfessor, nome))
            connection.commit()

        finally:
            connection.close()
            escolhas()

    elif nomeTabela == 'Prova':
        idProva = int(input("Entre o ID da prova: "))
        valorNota = int(input("Entre a nota da prova: "))
        grau = str(input("Entre o grau da prova (A/B): "))
        numMatricula = int(input("Entre o numero de matricula do aluno: "))
        idProfessor = int(input("Entre o ID do professor: "))
        sql_command = "INSERT INTO `Prova` (`IdProva`, `ValorNota`, `Grau`, `Aluno_NumeroMatricula`, `Professor_IdProfessor`) VALUES (%s, %s, %s, %s, %s)"
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql_command, (idProva, valorNota, grau, numMatricula, idProfessor))
            connection.commit()

        finally:
            connection.close()
            escolhas()


if __name__ == '__main__':
    escolhas()
